import glob
import sys
from math import hypot
from collections import defaultdict
from PIL import Image
import matplotlib.pyplot as plt
from matplotlib.widgets import RectangleSelector


x1, x2, y1, y2 = 0., 0., 0., 0.

def load_image(path):
    try:
        im = Image.open(path)
    except IOError as e:
        print("IO Error:\n{}".format(e))
        sys.exit(1)

    return im


def line_select_callback(eclick, erelease):
    'eclick and erelease are the press and release events'
    global x1, x2, y1, y2
    x1, y1 = eclick.xdata, eclick.ydata
    x2, y2 = erelease.xdata, erelease.ydata
    print("(%3.2f, %3.2f) --> (%3.2f, %3.2f)" % (x1, y1, x2, y2))
    print(" The button you used were: %s %s" % (eclick.button, erelease.button))


def toggle_selector(event):
    print(' Key pressed.')
    if event.key in ['Q', 'q'] and toggle_selector.RS.active:
        print(' RectangleSelector deactivated.')
        toggle_selector.RS.set_active(False)
    if event.key in ['A', 'a'] and not toggle_selector.RS.active:
        print(' RectangleSelector activated.')
        toggle_selector.RS.set_active(True)


def zoom_factory(ax,base_scale = 2.):
    def zoom_fun(event):
        # get the current x and y limits
        cur_xlim = ax.get_xlim()
        cur_ylim = ax.get_ylim()
        cur_xrange = (cur_xlim[1] - cur_xlim[0])*.5
        cur_yrange = (cur_ylim[1] - cur_ylim[0])*.5
        xdata = event.xdata # get event x location
        ydata = event.ydata # get event y location
        if event.button == 'up':
            # deal with zoom in
            scale_factor = 1/base_scale
        elif event.button == 'down':
            # deal with zoom out
            scale_factor = base_scale
        else:
            # deal with something that should never happen
            scale_factor = 1
        # set new limits
        ax.set_xlim([xdata - cur_xrange*scale_factor,
                     xdata + cur_xrange*scale_factor])
        ax.set_ylim([ydata - cur_yrange*scale_factor,
                     ydata + cur_yrange*scale_factor])
        plt.draw() # force re-draw

    fig = ax.get_figure() # get the figure of interest
    # attach the call back
    fig.canvas.mpl_connect('scroll_event',zoom_fun)

    #return the function
    return zoom_fun


def main():
    image_paths = glob.glob("samples/*")
    cycles = 2

    data = defaultdict(list)
    global x1, x2, y1, y2

    for _ in range(cycles):
        for pimg in image_paths:
            im = load_image(pimg) 
            fig, current_ax = plt.subplots()
            plt.imshow(im)
            toggle_selector.RS = RectangleSelector(current_ax, line_select_callback,
                                                   drawtype='box', useblit=True,
                                                   button=[1, 3],  
                                                   spancoords='pixels',
                                                   interactive=True)
            plt.connect('key_press_event', toggle_selector)
            f = zoom_factory(current_ax)
            plt.show()
            print("Done: x1 {}, y1 {}, x2 {}, y2 {}".format(x1, y1, x2, y2))

            data[pimg].append(hypot(x2 - x1, y2 - y1))
            x1, x2, y1, y2 = 0., 0., 0., 0.
   
    
    length = float(input("What length (in cm) did you measure?: "))
    scale = float(input("What length (in pixels) do you want it to become?: "))

    for pimg, lens in data.items():
        alen = sum(lens) / len(lens)
        x = scale / alen
        im = load_image(pimg) 
        nim = im.resize((int(im.size[0] * x), int(im.size[1] * x)))
        nim.save("{}/{}.jpg".format("scaled", pimg.split("/")[-1][:-4]))

if __name__ == "__main__":
    sys.exit(main())
